import heapq
from pathlib import Path
import sys
from typing import List, Tuple

NEIGHBOURS = ((-1, 0), (1, 0), (0, -1), (0, 1))

def h(
    grid: List[List[int]],
    position: Tuple[int, int],
    max_x: int,
    max_y: int
) -> int:
    x = max_x- position[0]
    y = max_y - position[1]
    return x + y

def dijkstra(grid: List[List[int]]) -> List[int]:
    closed = set()
    open = [(0, (0, 0))]
    dists = {(0, 0): 0}
    previous = {}
    max_x = len(grid[0]) - 1
    max_y = len(grid) - 1

    while open:
        cost, v = heapq.heappop(open)
        if v in closed:
            continue
        closed.add(v)

        if v == (max_x, max_y):
            break

        x, y = v
        for dx, dy in NEIGHBOURS:
            nx, ny = x + dx, y + dy
            if (nx, ny) in closed:
                continue
            if not 0 <= nx <= max_x or not 0 <= ny <= max_y:
                continue
            new_cost = dists[v] + grid[ny][nx]

            if (nx, ny) not in dists or new_cost < dists[(nx, ny)]:
                dists[(nx, ny)] = new_cost
                previous[(nx, ny)] = v
            heapq.heappush(
                open,
                (new_cost + h(grid, (nx, ny), max_y, max_y), (nx, ny))
            )

    path = []
    target = (max_x, max_y)
    while target != (0, 0):
        path.append(grid[target[1]][target[0]])
        target = previous[target]
    return path


def print_grid(grid: List[List[int]]):
    for y, row in enumerate(grid):
        if y % 10 == 0:
            print()
        for x, n in enumerate(row):
            if x % 10 == 0:
                print(' ', end='')
            print(n, end='')
        print()

def expand_grid(grid: List[List[int]], size: int) -> List[List[int]]:
    new_grid = [[None] * size * len(grid[0]) for _ in range(len(grid) * size)]
    for y, row in enumerate(grid):
        for x, cell in enumerate(row):
            for j in range(size):
                for i in range(size):
                    new_cell = cell + i + j
                    if new_cell > 9:
                        new_cell -= 9
                    new_grid[len(grid) * j + y][len(row) * i + x] = new_cell

    return new_grid

def main(infile: str):
    grid = [[int(c) for c in line]
        for line in Path(infile).read_text().splitlines()]

    print('Day 15 part 1:', sum(dijkstra(grid)))

    grid = expand_grid(grid, 5)
    print('Day 15 part 2:', sum(dijkstra(grid)))


if __name__ == '__main__':
    main(sys.argv[1])
