from collections import defaultdict
import copy
from pathlib import Path
import sys
from typing import Dict, List, Tuple

Image = Dict[Tuple[int, int], int]

NEIGHBOURS = (
    (1, 1), (0, 1), (-1, 1),
    (1, 0), (0, 0), (-1, 0),
    (1, -1), (0, -1), (-1, -1),
)

def print_image(image: Image):
    for row in image:
        for p in row:
            print('#' if p else ' ', end='')
        print()


def convert_line(line: str) -> List[int]:
    return [1 if c == '#' else 0 for c in line]


def enhance(algorithm: List[int], image: Image) -> Image:
    new_image = copy.deepcopy(image)
    print('size', len(image), len(image[0]))
    print_image(image)
    for y in range(1, len(image) - 1):
        for x in range(1, len(image[0]) - 1):
            idx = 0
            for i, d in enumerate(NEIGHBOURS):
                idx += 2 ** i * image[y + d[1]][x + d[0]]
            new_image[y][x] = algorithm[idx]
    return new_image


def extend(image: Image) -> Image:
    new_image = []

    new_image.append([0] * (len(image[0]) + 2))
    for row in image:
        new_image.append([0] + row + [0])
    new_image.append([0] * (len(image[0]) + 2))

    return new_image

def shrink(image: Image) -> Image:
    return [line[:] for line in image[:]]


def main(infile: str):
    lines = Path(infile).read_text().splitlines()
    algorithm = convert_line(lines[0].strip())
    image = [convert_line(l.strip()) for l in lines[2:]]

    print_image(image)
    for i in range(2):
        image = enhance(algorithm, extend(image))
        print_image(image)

    print('Day 20 part 1:', sum(sum(r) for r in image))

if __name__ == '__main__':
    main(sys.argv[1])
