from collections import defaultdict
from copy import copy
import heapq
from pathlib import Path
import string
import sys
from typing import DefaultDict, Iterator, List, Tuple

ROOM_ENTRY = {'A': 2, 'B': 4, 'C': 6, 'D': 8}
ROOM_MAP = {'A': 0, 'B': 1, 'C': 2, 'D': 3}
COST_MAP = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}

Rooms = List[List[str]]
Hallway = List[bool]

def deepcopy(l: Rooms):
    return [copy(e) for e in l]

def is_correct(rooms: Rooms, id: int, roomsize: int) -> bool:
    return rooms[id] == [sorted(ROOM_MAP.keys())[id]] * roomsize


def is_goal(rooms: Rooms, roomsize: int) -> bool:
    return is_correct(rooms, 0, roomsize) and is_correct(rooms, 1, roomsize) \
        and is_correct(rooms, 2, roomsize) and is_correct(rooms, 3, roomsize)


def neighbours(
    rooms: Rooms,
    hallway: Hallway,
    roomsize: int
) -> Iterator[Tuple[Rooms, Hallway, int]]:
    # Try moving to room first
    for i, letter in enumerate(hallway):
        if not letter:
            continue
        if len(rooms[ROOM_MAP[letter]]) < roomsize:
            open = True
            for other in rooms[ROOM_MAP[letter]]:
                if other != letter:
                    open = False

            if not open:
                continue

            blocked = False
            if ROOM_ENTRY[letter] < i:
                for k in range(ROOM_ENTRY[letter], i):
                    if hallway[k]:
                        blocked = True
                        break
            else:
                for k in range(i + 1, ROOM_ENTRY[letter]):
                    if hallway[k]:
                        blocked = True
                        break

            if blocked:
                continue

            new_rooms = deepcopy(rooms)
            new_hallway = hallway[:]

            new_rooms[ROOM_MAP[letter]].append(letter)
            new_hallway[i] = False

            cost = (abs(i - ROOM_ENTRY[letter]) + roomsize - len(rooms[ROOM_MAP[letter]])) * COST_MAP[letter]
            yield new_rooms, new_hallway, cost
    # Move out of rooms
    for i in range(4):
        if not rooms[i] or is_correct(rooms, i, roomsize):
            continue
        if all(o == sorted(ROOM_MAP.keys())[i] for o in rooms[i]):
            continue

        room = (i + 1) * 2
        new_rooms = deepcopy(rooms)
        letter = new_rooms[i].pop()
        for j in range(11):
            if j in ROOM_ENTRY.values():
                continue

            a, b = min(room, j), max(room, j)
            for k in range(a, b + 1):
                if hallway[k]:
                    break
            else:
                new_hallway = hallway[:]
                new_hallway[j] = letter
                cost = (abs(room - j) + roomsize - len(new_rooms[i])) * COST_MAP[letter]
                yield new_rooms, new_hallway, cost


def state_hash(rooms: Rooms, hallway: Hallway) -> str:
    return str(rooms) + str(hallway)


def h(rooms: Rooms, hallway: Hallway) -> int:
    predicted_cost = 0
    for i, room in enumerate(rooms):
        for letter in room:
            dist = abs((i + 1) * 2 - ROOM_ENTRY[letter])
            predicted_cost += dist * COST_MAP[letter]
    for i, letter in enumerate(hallway):
        if letter:
            dist = abs(i - ROOM_ENTRY[letter])
            predicted_cost += dist * COST_MAP[letter]
    return predicted_cost


def astar(rooms: Rooms, hallway: Hallway, roomsize: int) -> int:
    open = [(0, 0, rooms, hallway)]
    scores = {state_hash(rooms, hallway): 0}

    i = 0
    while open:
        _, _, rooms, hallway = heapq.heappop(open)
        if is_goal(rooms, roomsize):
            return scores[state_hash(rooms, hallway)]

        score = scores[state_hash(rooms, hallway)]

        for n_rooms, n_hallway, n_cost in neighbours(rooms, hallway, roomsize):
            i += 1
            neighbour_score = score + n_cost
            n_idx = state_hash(n_rooms, n_hallway)
            if (n_idx not in scores or neighbour_score < scores[n_idx]):
                scores[n_idx] = neighbour_score
                heapq.heappush(open, (
                    neighbour_score + h(n_rooms, n_hallway),
                    i,
                    n_rooms,
                    n_hallway
                ))

    return -1


def main(infile):
    rooms = [[], [], [], []]
    hallway = [False] * 11
    for line in reversed(Path(infile).read_text().splitlines()[-3:-1]):
        letters = filter(lambda l: l in string.ascii_uppercase, line)
        for i, letter in enumerate(letters):
            rooms[i].append(letter)

    print('Day 23 part 1:', astar(deepcopy(rooms), hallway, 2))

    rooms[0] = [rooms[0][0]] + ['D', 'D'] + [rooms[0][1]]
    rooms[1] = [rooms[1][0]] + ['B', 'C'] + [rooms[1][1]]
    rooms[2] = [rooms[2][0]] + ['A', 'B'] + [rooms[2][1]]
    rooms[3] = [rooms[3][0]] + ['C', 'A'] + [rooms[3][1]]
    print('Day 23 part 2:', astar(rooms, hallway, 4))


if __name__ == '__main__':
    main(sys.argv[1])
