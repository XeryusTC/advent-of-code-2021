import copy
from pathlib import Path
import sys
from typing import Set

def fold_x(dots: Set[int], coord: int) -> Set[int]:
    result = set()
    for dot in dots:
        if dot[0] < coord:
            result.add(dot)
        else:
            dx = dot[0] - coord
            result.add((coord - dx, dot[1]))

    return result

def fold_y(dots: Set[int], coord: int) -> Set[int]:
    result = set()
    for dot in dots:
        if dot[1] < coord:
            result.add(dot)
        else:
            dy = dot[1] - coord
            result.add((dot[0], coord - dy))

    return result


def print_dots(dots: Set[int]):
    max_x = max(dot[0] for dot in dots) + 1
    max_y = max(dot[1] for dot in dots) + 1
    for y in range(max_y):
        for x in range(max_x):
            if (x, y) in dots:
                print('#', end='')
            else:
                print('.', end='')
        print()

def main(infile: str):
    input = Path(infile).read_text().splitlines()
    dots = set()
    folds = []
    for line in input:
        if ',' in line:
            x, y = line.split(',', 1)
            dots.add((int(x), int(y)))
        if line.startswith('fold along'):
            axis = line[11]
            coord = int(line[13:])
            folds.append((axis, coord))

    if folds[0][0] == 'x':
        part1 = fold_x(dots, folds[0][1])
    else:
        part1 = fold_y(dots, folds[0][1])
    print('Day 13 part 1:', len(part1))

    for fold in folds:
        if fold[0] == 'x':
            dots = fold_x(dots, fold[1])
        else:
            dots = fold_y(dots, fold[1])

    print('Day 13 part 2:')
    print_dots(dots)


if __name__ == '__main__':
    main(sys.argv[1])
