from pathlib import Path
import sys

NEIGHBOURS = [(-1, -1), (0, -1), (1, -1),
              (-1, 0),           (1, 0),
              (-1, 1),  (0, 1),  (1, 1)]

WIDTH = 10
HEIGHT = 10


def print_octopuses(octopuses):
    for y in range(HEIGHT):
        for x in range(WIDTH):
            print(octopuses[(x, y)], end=' ')
        print()


def main(infile: str):
    octopuses_input = [[int(c) for c in line]
        for line in Path(infile).read_text().splitlines()]

    octopuses = {}
    for y in range(len(octopuses_input)):
        for x in range(len(octopuses_input[0])):
            octopuses[(x, y)] = octopuses_input[y][x]

    num_flashes = 0
    for i in range(1000):
        octopuses = {idx: o + 1 for (idx, o) in octopuses.items()}
        flashed = {idx: False for idx in octopuses}
        while any(v > 9 for v in octopuses.values()):
            for x, y in octopuses:
                if octopuses[(x, y)] > 9 and not flashed[(x, y)]:
                    octopuses[(x, y)] = 0
                    flashed[(x, y)] = True
                    if i < 100:
                        num_flashes += 1

                    for nx, ny in NEIGHBOURS:
                        try:
                            octopuses[(x + nx, y + ny)] += 1
                        except KeyError:
                            continue

        if all(flashed.values()):
            break

        for idx in octopuses:
            if flashed[idx]:
                octopuses[idx] = 0

    print('Day 11 part 1:', num_flashes)
    print('Day 11 part 2:', i + 1)

if __name__ == '__main__':
    main(sys.argv[1])
