from collections import Counter, defaultdict
from operator import itemgetter
from pathlib import Path
import sys
from typing import Dict, List, Tuple

def insert(pairs: Dict[str, int], rules: Dict[str, str]) -> str:
    result = defaultdict(int)
    for pair, count in pairs.items():
        inserted = rules[pair]
        result[pair[0] + inserted] += count
        result[inserted + pair[1]] += count
    return result


def count_elements(pairs: Dict[str, int]) -> List[Tuple[str, int]]:
    counts = defaultdict(int)
    for pair, count in pairs.items():
        counts[pair[0]] += count
    return list(sorted(counts.items(), key=itemgetter(1)))


def main(infile: str):
    input = Path(infile).read_text().splitlines()
    polymer = input[0].strip()
    pairs = zip(polymer, polymer[1:])
    pairs = dict(Counter(pair[0] + pair[1] for pair in pairs).most_common())
    rules = {}
    for line in input[2:]:
        pair, result = line.split('->', 1)
        rules[pair.strip()] = result.strip()

    for i in range(10):
        pairs = insert(pairs, rules)
    counts = count_elements(pairs)
    print('Day 14 part 1:', counts[-1][1] - counts[0][1] + 1)


    for i in range(30):
        pairs = insert(pairs, rules)
    counts = count_elements(pairs)
    print('Day 14 part 2:', counts[-1][1] - counts[0][1] + 1)


if __name__ == '__main__':
    main(sys.argv[1])
