from collections import deque
from functools import cache, lru_cache, partial
from itertools import combinations, product
from pathlib import Path
import sys
from typing import Any, Iterator, List, Tuple

Coordinate = Tuple[int, int, int]
sin = [0, 1, 0, -1]
cos = [1, 0, -1, 0]

def generate_matrices():
    matrices = set()

    for x in range(4):
        for y in range(4):
            for z in range(4):
                matrices.add((
                    (
                        cos[x] * cos[y],
                        cos[x] * sin[y] * sin[z] - sin[x] * cos[z],
                        cos[x] * sin[y] * cos[z] + sin[x] * sin[z],
                    ),
                    (
                        sin[x] * cos[y],
                        sin[x] * sin[y] * sin[z] + cos[x] * cos[z],
                        sin[x] * sin[y] * cos[z] - cos[x] * sin[z],
                    ),
                    (-sin[y], cos[y] * sin[z], cos[y] * cos[z])
                ))

    return matrices


MATRICES = generate_matrices()


def rotate_scanner(
    scanner: List[Coordinate]
) -> Tuple[Any, Iterator[List[Coordinate]]]:
    for R in MATRICES:
        rotate = partial(rotate_coordinate, R)
        yield R, list(map(rotate, scanner))


def rotate_coordinate(R, coord: Coordinate) -> Coordinate:
    return (
        R[0][0] * coord[0] + R[0][1] * coord[1] + R[0][2] * coord[2],
        R[1][0] * coord[0] + R[1][1] * coord[1] + R[1][2] * coord[2],
        R[2][0] * coord[0] + R[2][1] * coord[1] + R[2][2] * coord[2],
    )


def rotate_matrix(R1, R2):
    return (
        (
            R1[0][0] * R2[0][0] + R1[0][1] * R2[1][0] + R1[0][2] * R2[2][0],
            R1[0][0] * R2[0][1] + R1[0][1] * R2[1][1] + R1[0][2] * R2[2][1],
            R1[0][0] * R2[0][2] + R1[0][1] * R2[1][2] + R1[0][2] * R2[2][2],
        ),
        (
            R1[1][0] * R2[0][0] + R1[1][1] * R2[1][0] + R1[1][2] * R2[2][0],
            R1[1][0] * R2[0][1] + R1[1][1] * R2[1][1] + R1[1][2] * R2[2][1],
            R1[1][0] * R2[0][2] + R1[1][1] * R2[1][2] + R1[1][2] * R2[2][2],
        ),
        (
            R1[2][0] * R2[0][0] + R1[2][1] * R2[1][0] + R1[2][2] * R2[2][0],
            R1[2][0] * R2[0][1] + R1[2][1] * R2[1][1] + R1[2][2] * R2[2][1],
            R1[2][0] * R2[0][2] + R1[2][1] * R2[1][2] + R1[2][2] * R2[2][2],
        ),
    )


def overlap(scanner1: List[Coordinate], scanner2: List[Coordinate]) -> Coordinate:
    # Short circuit based on distances between all pairs of beacons in a scanner
    # 66 = len(combinations(12, 2))
    scanner1 = list(sorted(scanner1))
    scanner2 = list(sorted(scanner2))
    dists1 = set(manhattan(a, b) for (a, b) in product(scanner1, scanner1))
    dists2 = set(manhattan(a, b) for (a, b) in product(scanner2, scanner2))
    if len(dists1.intersection(dists2)) < 66:
        return None

    offsets = set(map(
        lambda c: (c[1][0] - c[0][0], c[1][1] - c[0][1], c[1][2] - c[0][2],),
        product(scanner1, scanner2)
    ))
    for offset in offsets:
        coords = set(map(
            lambda c: (c[0] + offset[0], c[1] + offset[1], c[2] + offset[2]),
            scanner1
        ))
        if len(coords.intersection(set(scanner2))) >= 12:
            return offset


def manhattan(coord1: Coordinate, coord2: Coordinate) -> int:
    return abs(coord1[0] - coord2[0]) + abs(coord1[1] - coord2[1]) \
        + abs(coord1[2] - coord2[2])


def main(infile: str):
    lines = Path(infile).read_text().splitlines()
    scanners = {}
    scanner_id = 0
    for line in lines:
        if line.startswith('---'):
            parts = line.split()
            scanner_id = int(parts[2])
            scanners[scanner_id] = []
        elif line:
            x, y, z = (int(c) for c in line.split(','))
            scanners[scanner_id].append((x, y, z))

    corrected_scanners = {0: scanners[0]}
    offsets = {0: (0, 0, 0)}
    rotations = {0: ((1, 0, 0), (0, 1, 0), (0, 0, 1))}

    corrected_ids = set([0])
    while corrected_ids:
        c_id = corrected_ids.pop()

        c_offset = offsets[c_id]
        c_coords = scanners[c_id]
        c_rotate = rotations[c_id]

        for id, scanner in scanners.items():
            if id in corrected_scanners or id == c_id:
                continue

            for R, rotated in rotate_scanner(scanner):
                offset = overlap(c_coords, rotated)
                if offset is not None:
                    offset2 = rotate_coordinate(c_rotate, offset)

                    corrected_scanners[id] = [
                        rotate_coordinate(c_rotate, s) for s in rotated
                    ]
                    offsets[id] = (
                        c_offset[0] - offset2[0],
                        c_offset[1] - offset2[1],
                        c_offset[2] - offset2[2],
                    )

                    rotations[id] = rotate_matrix(c_rotate, R)

                    corrected_ids.add(id)
                    break

    beacons = set()
    for id, scans in corrected_scanners.items():
        offset = offsets[id]
        scans = map(
            lambda s: (s[0] + offset[0], s[1] + offset[1], s[2] + offset[2]),
            scans
        )
        beacons |= set(scans)
    print('Day 19 part 1:', len(beacons))

    print('Day 19 part 2:',
        max(manhattan(c1, c2) for c1, c2 in combinations(offsets.values(), 2)))


if __name__ == '__main__':
    main(sys.argv[1])
