from functools import reduce
from pathlib import Path
import sys

def hex_to_decimal(packet: str) -> str:
    result = ''
    for c in packet:
        result += f'{int(c, 16):04b}'
    return result


class Packet:
    def __init__(self, binary: str):
        self.version = int(binary[:3], 2)
        self.type = int(binary[3:6], 2)
        self.length = 6
        self.subpackets = []

        if self.type == 4:
            number_bits = ''
            keep_parsing = True
            while keep_parsing:
                if binary[self.length] == '0':
                    keep_parsing = False
                number_bits += binary[self.length + 1:self.length + 5]
                self.length += 5
            self.value = int(number_bits, 2)
        else:
            length_type = binary[self.length]
            self.length += 1
            if length_type == '0':
                bit_length = int(binary[self.length:self.length + 15], 2)
                self.length += 15

                while sum(sp.length for sp in self.subpackets) < bit_length:
                    subpacket = Packet(binary[self.length:])
                    self.subpackets.append(subpacket)
                    self.length += subpacket.length
            else:
                num_subpackets = int(binary[self.length:self.length + 11], 2)
                self.length += 11

                for _ in range(num_subpackets):
                    subpacket = Packet(binary[self.length:])
                    self.subpackets.append(subpacket)
                    self.length += subpacket.length

    def sum_version(self) -> int:
        subpacket_sum = sum(sp.sum_version() for sp in self.subpackets)
        return self.version + subpacket_sum

    def packet_value(self) -> int:
        sp_gen = (sp.packet_value() for sp in self.subpackets)
        if self.type == 0:
            return sum(sp_gen)
        elif self.type == 1:
            return reduce(lambda a, b: a * b, sp_gen, 1)
        elif self.type == 2:
            return min(sp_gen)
        elif self.type == 3:
            return max(sp_gen)
        elif self.type == 4:
            return self.value
        elif self.type == 5:
            pv0 = self.subpackets[0].packet_value()
            pv1 = self.subpackets[1].packet_value()
            return 1 if pv0 > pv1 else 0
        elif self.type == 6:
            pv0 = self.subpackets[0].packet_value()
            pv1 = self.subpackets[1].packet_value()
            return 1 if pv0 < pv1 else 0
        elif self.type == 7:
            pv0 = self.subpackets[0].packet_value()
            pv1 = self.subpackets[1].packet_value()
            return 1 if pv0 == pv1 else 0

    def __str__(self) -> str:
        result = f'V={self.version}, T={self.type}, len={self.length}'
        if self.type == 4:
            result += f', v={self.value}'
        return result


def main(infile: str):
    packets = Path(infile).read_text().splitlines()
    for packet in packets:
        bin_packet = hex_to_decimal(packet)
        parsed_packet = Packet(bin_packet)
        print('Day 16 part 1:', parsed_packet.sum_version())
        print('Day 16 part 2:', parsed_packet.packet_value())


if __name__ == '__main__':
    main(sys.argv[1])
