from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from itertools import chain
from pathlib import Path
from pprint import pprint
import re
import sys
from typing import Dict, List, Optional, Tuple

Mutation = namedtuple('Mutation',
    ['toggle', 'min_x', 'max_x', 'min_y', 'max_y', 'min_z', 'max_z'])

RE = re.compile(
    r'(\w+) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)'
)

@dataclass(unsafe_hash=True)
class Region:
    min_x: int
    max_x: int
    min_y: int
    max_y: int
    min_z: int
    max_z: int
    state: bool = False

    def contains(self, other: "Region") -> bool:
        return (
            self.min_x <= other.min_x and self.max_x >= other.max_x
            and self.min_y <= other.min_y and self.max_y >= other.max_y
            and self.min_z <= other.min_z and self.max_z >= other.max_z
        )

    def count(self) -> int:
        if not self.state:
            return 0
        return ((self.max_x - self.min_x + 1)
            * (self.max_y - self.min_y + 1)
            * (self.max_z - self.min_z + 1))

    def intersect(self, other) -> bool:
        return (
            self.max_x >= other.min_x and self.min_x <= other.max_x
            and self.max_y >= other.min_y and self.min_y <= other.max_y
            and self.max_z >= other.min_z and self.min_z <= other.max_z
        )

    def split_x(self, x1, x2) -> List["Region"]:
        result = []
        if x1 > self.min_x:
            result.append(Region(
                self.min_x, x1 - 1,
                self.min_y, self.max_y,
                self.min_z, self.max_z, self.state
            ))
            remainder = Region(
                x1, self.max_x,
                self.min_y, self.max_y,
                self.min_z, self.max_z, self.state
            )
        else:
            remainder = self

        if x2 < self.max_x:
            result += [
                Region(
                    remainder.min_x, x2,
                    self.min_y, self.max_y,
                    self.min_z, self.max_z, self.state
                ),
                Region(
                    x2 + 1, self.max_x,
                    self.min_y, self.max_y,
                    self.min_z, self.max_z, self.state
                )
            ]
        else:
            result.append(remainder)
        return result

    def split_y(self, y1, y2) -> List["Region"]:
        result = []
        if y1 > self.min_y:
            result.append(Region(
                self.min_x, self.max_x,
                self.min_y, y1 - 1,
                self.min_z, self.max_z, self.state
            ))
            remainder = Region(
                self.min_x, self.max_x,
                y1, self.max_y,
                self.min_z, self.max_z, self.state
            )
        else:
            remainder = self

        if y2 < self.max_y:
            result += [
                Region(
                    self.min_x, self.max_x,
                    remainder.min_y, y2,
                    self.min_z, self.max_z, self.state
                ),
                Region(
                    self.min_x, self.max_x,
                    y2 + 1, self.max_y,
                    self.min_z, self.max_z, self.state
                )
            ]
        else:
            result.append(remainder)
        return result

    def split_z(self, z1, z2) -> List["Region"]:
        result = []
        if z1 > self.min_z:
            result.append(Region(
                self.min_x, self.max_x,
                self.min_y, self.max_y,
                self.min_z, z1 - 1, self.state
            ))
            remainder = Region(
                self.min_x, self.max_x,
                self.min_y, self.max_y,
                z1, self.max_z, self.state
            )
        else:
            remainder = self

        if z2 < self.max_z:
            result += [
                Region(
                    self.min_x, self.max_x,
                    self.min_y, self.max_y,
                    remainder.min_z, z2, self.state
                ),
                Region(
                    self.min_x, self.max_x,
                    self.min_y, self.max_y,
                    z2 + 1, self.max_z, self.state
                )
            ]
        else:
            result.append(remainder)
        return result


def main(infile: str):
    lines = [line.strip() for line in Path(infile).read_text().splitlines()]
    grid = defaultdict(int)
    regions = set()
    for line in lines:
        result = RE.match(line)
        mutation = Mutation(result.group(1) == 'on',
            *(int(g) for g in result.groups()[1:]))

        # Part 1
        min_x = max(-50, mutation.min_x)
        min_y = max(-50, mutation.min_y)
        min_z = max(-50, mutation.min_z)
        max_x = min(50, mutation.max_x)
        max_y = min(50, mutation.max_y)
        max_z = min(50, mutation.max_z)
        for x in range(min_x, max_x + 1):
            for y in range(min_y, max_y + 1):
                for z in range(min_z, max_z + 1):
                    if mutation.toggle:
                        grid[x, y, z] = 1
                    else:
                        grid[x, y, z] = 0

        # Part 2
        region = Region(
            *(int(g) for g in result.groups()[1:]),
            result.group(1) == 'on'
        )
        # Remove regions completely contained in this one
        to_remove = set()
        for other in regions:
            if region.contains(other):
                to_remove.add(other)
        regions -= to_remove

        to_remove = set()
        to_add = set()
        for other in regions:
            if region.intersect(other):

                new = other.split_x(region.min_x, region.max_x)
                new = chain(*[o.split_y(region.min_y, region.max_y)
                    for o in new])
                new = chain(*[o.split_z(region.min_z, region.max_z)
                    for o in new])
                to_add |= set(filter(lambda o: not region.intersect(o), new))

                to_remove.add(other)

        regions -= to_remove
        regions |= to_add

        regions.add(region)

    print('Day 22 part 1:', sum(grid.values()))
    print('Day 22 part 2:', sum(r.count() for r in regions))


if __name__ == '__main__':
    main(sys.argv[1])
