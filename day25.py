from pathlib import Path
import sys
from typing import Set, Tuple

Coord = Tuple[int, int]

def move_right(
    right: Set[Coord],
    down: Set[Coord],
    width: int,
) -> Tuple[bool, Set[Coord]]:
    result = set()
    changes = False
    for x, y in right:
        next = ((x + 1) % width, y)
        if next not in right and next not in down:
            result.add(next)
            changes = True
        else:
            result.add((x, y))
    return changes, result


def move_down(
    right: Set[Coord],
    down: Set[Coord],
    height: int,
) -> Tuple[bool, Set[Coord]]:
    result = set()
    changes = False
    for x, y in down:
        next = (x, (y + 1) % height)
        if next not in right and next not in down:
            result.add(next)
            changes = True
        else:
            result.add((x, y))
    return changes, result


def print_world(width: int, height: int, right: Set[Coord], down: Set[Coord]):
    for y in range(height):
        for x in range(width):
            if (x, y) in right:
                print('>', end='')
            elif (x, y) in down:
                print('v', end='')
            else:
                print('.', end='')
        print()

def main(infile: str):
    right = set()
    down = set()
    for y, line in enumerate(Path(infile).read_text().splitlines()):
        for x, c in enumerate(line):
            if c == '>':
                right.add((x, y))
            elif c == 'v':
                down.add((x, y))
    width = x + 1
    height = y + 1

    right_changes = True
    down_changes = True
    i = 0
    while right_changes or down_changes:
        right_changes, right = move_right(right, down, width)
        down_changes, down = move_down(right, down, height)
        i += 1

    print('Day 25:', i)


if __name__ == '__main__':
    main(sys.argv[1])
