import math
from pathlib import Path
import sys
from typing import List, Tuple, Union

Number = List[Union[str, int]]

def tokenize(number: str) -> Number:
    tokens = []
    for c in number:
        if c == ',':
            continue
        if c in '[]':
            tokens.append(c)
        else:
            tokens.append(int(c))
    return tokens

def number2str(number: Number) -> str:
    if isinstance(number, int):
        return number

    result = ''
    for a, b in zip(number, number[1:]):
        result += str(a)
        if (a == ']' and b == '['
            or a == ']' and isinstance(b, int)
            or isinstance(a, int) and b != ']'):
            result += ', '
    result += ']'
    return result


def explode(number: Number) -> Tuple[bool, Number]:
    level = 0
    for i, c in enumerate(number):
        if c == '[':
            level += 1
        if c == ']':
            level -= 1

        if level == 5:
            left, right = number[i + 1], number[i + 2]
            for j in range(i):
                if isinstance(number[i - j], int):
                    number[i - j] += left
                    break
            number = number[:i] + [0] + number[i + 4:]
            for j in range(i + 1, len(number)):
                if isinstance(number[j], int):
                    number[j] += right
                    break

            return True, number
    return False, number


def split(number: Number) -> Tuple[bool, Number]:
    for i, c in enumerate(number):
        if isinstance(c, int) and c >= 10:
            new_number = number[:i] + \
                ['[', math.floor(c / 2), math.ceil(c / 2), ']'] + \
                number[i+1:]
            return True, new_number
    return False, number


def simplify(number: Number) -> Tuple[bool, Number]:
    exploded, number = explode(number)
    if exploded:
        return True, number

    splitted, number = split(number)
    if splitted:
        return True, number
    return False, number


def add(n1: Number, n2: Number) -> Number:
    new_number = ['['] + n1 + n2 + [']']
    simplified = True
    i = 0
    while simplified or i < 3:
        simplified, new_number = simplify(new_number)
        i += 1
    return new_number


def magnitude(number: Number) -> int:
    if isinstance(number, int):
        return number

    level = 0
    left = []
    right = []
    for i, c in enumerate(number):
        if c == '[':
            level += 1
        elif c == ']':
            level -= 1
            if level == 1:
                left = number[1:i + 1]
                break
        else:
            if level == 1:
                left = c
                break
    level = 0
    for i, c in enumerate(reversed(number)):
        if c == ']':
            level += 1
        elif c == '[':
            level -= 1
            if level == 1:
                right = number[-i -1:-1]
                break
        else:
            if level == 1:
                right = c
                break

    return 3 * magnitude(left) + 2 * magnitude(right)


def main(infile: str):
    lines = [tokenize(l) for l in Path(infile).read_text().splitlines()]
    number = lines[0]
    for to_add in lines[1:]:
        number = add(number, to_add)
    print('Day 18 part 1:', magnitude(number))

    max_magnitude = 0
    for i, a in enumerate(lines):
        for j, b in enumerate(lines):
            if i == j:
                continue
            mag = magnitude(add(a, b))
            if mag > max_magnitude:
                max_magnitude = mag
    print('Day 18 part 2:', max_magnitude)



if __name__ == '__main__':
    main(sys.argv[1])
