from pathlib import Path
import sys
from typing import List

def calculate_cost(target: int, positions: List[int]) -> int:
    return sum(map(lambda s: abs(target - s), positions))


def calculate_new_cost(target: int, positions: List[int]) -> int:
    cost = 0
    for position in positions:
        dist = abs(target - position)
        step_cost = (dist * (1 + dist)) / 2
        cost += step_cost
    return int(cost)


def main(infile: str):
    positions = list(map(int, Path(infile).read_text().split(',')))
    print('Day 7 part 1:', min(calculate_cost(i, positions)
        for i in range(min(positions), max(positions) + 1)))
    print('Day 7 part 2:', min(calculate_new_cost(i, positions)
        for i in range(min(positions), max(positions) + 1)))


if __name__ == '__main__':
    main(sys.argv[1])
