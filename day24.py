from itertools import chain
from pathlib import Path
import sys
from typing import Callable

def step(mod_offset: int, y_offset: int, z_div: int) -> Callable:
    def func(z: int, input: int) -> int:
        if (z % 26) + mod_offset == input:
            return int(z / z_div)
        else:
            return int(z / z_div) * 26 + (input + y_offset)

    return func


steps = [
    step( 14,  8,  1),
    step( 15, 11,  1),
    step( 13,  2,  1),
    step(-10, 11, 26),
    step( 14,  1,  1),
    step( -3,  5, 26),
    step(-14, 10, 26),
    step( 12,  6,  1),
    step( 14,  1,  1),
    step( 12, 11,  1),
    step( -6,  9, 26),
    step( -6, 14, 26),
    step( -2, 11, 26),
    step( -9,  2, 26),
]


def main(infile: str):
    max_z = {0: ()}
    min_z = {0: ()}
    idx = 0
    while idx < 14:
        next_z = {}
        next_min = {}
        for z, prefix in chain(max_z.items(), min_z.items()):
            for i in range(9, 0, -1):
                current = prefix + (i,)
                result = steps[idx](z, i)
                if result not in next_z or next_z[result] < current:
                    next_z[result] = current
                if result not in next_min or next_min[result] > current:
                    next_min[result] = current
        print(f'Gen {idx:>2} size {len(next_z)}')
        max_z = next_z
        min_z = next_min
        idx += 1
    print(len(max_z[0]), max_z[0])
    print(len(min_z[0]), min_z[0])

    print('Day 24 part 1:', ''.join(str(d) for d in max_z[0]))
    print('Day 24 part 2:', ''.join(str(d) for d in min_z[0]))


if __name__ == '__main__':
    main(sys.argv[1])
