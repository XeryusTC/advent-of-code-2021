from itertools import cycle, islice
from pathlib import Path
import sys
from typing import Iterator, List

QUANTUM = {
    3: 1,
    4: 3,
    5: 6,
    6: 7,
    7: 6,
    8: 3,
    9: 1,
}
cache = {}

def take(n, iterable):
    return list(islice(iterable, n))


def deterministic_die() -> Iterator[int]:
    for roll in cycle(range(1, 101)):
        yield roll


def play(positions: List[int], rolls: Iterator[int]) -> int:
    scores = [0, 0]
    current = 0
    total_rolls = 0
    while True:
        for _ in range(3):
            roll = next(rolls)
            positions[current] = (positions[current] + roll) % 10
            total_rolls += 1

        scores[current] += positions[current] + 1

        if scores[current] >= 1000:
            return scores, total_rolls

        current = (current + 1) % 2


def play2(
    positions: List[int],
    scores: List[int],
    current: int=0
) -> List[int]:
    key = (tuple(positions), tuple(scores), current)
    if key in cache:
        return cache[key]

    result = [0, 0]
    for roll in range(3, 10):
        new_positions = positions[:]
        new_positions[current] = (new_positions[current] + roll) % 10
        new_scores = scores[:]
        new_scores[current] += new_positions[current] + 1

        if new_scores[current] >= 21:
            result[current] += QUANTUM[roll]
            continue

        wins = play2(new_positions, new_scores, (current + 1) % 2)
        cache[tuple(new_positions), tuple(new_scores), (current + 1) % 2] = wins

        result[0] += wins[0] * QUANTUM[roll]
        result[1] += wins[1] * QUANTUM[roll]

    return result


def main(infile: str):
    positions = [int(p[-1]) - 1 for p in Path(infile).read_text().splitlines()]
    scores, total_rolls = play(positions[:], deterministic_die())
    print('Day 21 part 1:', min(scores) * total_rolls)

    player1, player2 = play2(positions, [0, 0])
    print('Day 21 part 2:', max(player1, player2))


if __name__ == '__main__':
    main(sys.argv[1])
