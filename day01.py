from pathlib import Path
import sys

def main(infile):
    input_ = Path(infile).read_text()

    depths = list(map(lambda x: int(x), input_.split()))
    increases = 0
    previous = None
    for depth in depths:
        if previous is None:
            previous = depth
            continue
        if previous < depth:
            increases += 1

        previous = depth

    print(f'Day 1 part 1: {increases}')

    windows = list(zip(depths, depths[1:], depths[2:]))
    sums = list(map(sum, windows))
    increases = filter(lambda a: a[0] < a[1], zip(sums, sums[1:]))
    print(f'Day 1 part 2: {len(list(increases))}')


if __name__ == '__main__':
    main(*sys.argv[1:])
