from pathlib import Path
import re
import sys
from typing import Tuple

def hits_target(
    min_x: int,
    max_x: int,
    min_y: int,
    max_y: int,
    vel_x: int,
    vel_y: int
) -> Tuple[bool, int]:
    x, y = 0, 0
    highest = -100
    while y > min_y and x < max_x and (vel_x > 0 or min_x <= x <= max_x):
        x += vel_x
        y += vel_y
        vel_x = max(0, vel_x - 1)
        vel_y -= 1

        highest = max(highest, y)

        if min_x <= x <= max_x and min_y <= y <= max_y:
            return True, highest
    return False, 0

def main(infile: str):
    _, _, x, y = Path(infile).read_text().split()
    xgroups = re.match(r'.=([-0-9]+)\.\.([-0-9]+)', x)
    ygroups = re.match(r'.=([-0-9]+)\.\.([-0-9]+)', y)
    min_x = int(xgroups.group(1))
    max_x = int(xgroups.group(2))
    min_y = int(ygroups.group(1))
    max_y = int(ygroups.group(2))

    highest = 0
    count = 0
    for x in range(1, max_x + 1):
        for y in range(min_y, -min_y):
            success, height = hits_target(min_x, max_x, min_y, max_y, x, y)
            if success:
                highest = max(highest, height)
                count += 1
    print('Day 17 part 1:', highest)
    print('Day 17 part 2:', count)


if __name__ == '__main__':
    main(sys.argv[1])
