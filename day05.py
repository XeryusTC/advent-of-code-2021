from collections import defaultdict, namedtuple
from pathlib import Path
import re
import sys

COORD_REGEX = re.compile(r'(\d+),(\d+) -> (\d+),(\d+)')
Line = namedtuple('Line', ['x1', 'y1', 'x2', 'y2'])


def main(infile: str):
    coordinates = []
    with Path(infile).open() as f:
        for line in f.readlines():
            m = COORD_REGEX.match(line.strip())
            if m:
                coord = Line(
                    int(m.group(1)),
                    int(m.group(2)),
                    int(m.group(3)),
                    int(m.group(4))
                )
                coordinates.append(coord)

    grid = defaultdict(int)
    for coord in coordinates:
        if coord.x1 == coord.x2:
            y1 = min(coord.y1, coord.y2)
            y2 = max(coord.y1, coord.y2)
            for y in range(y1, y2 + 1):
                grid[(coord.x1, y)] += 1
        elif coord.y1 == coord.y2:
            x1 = min(coord.x1, coord.x2)
            x2 = max(coord.x1, coord.x2)
            for x in range(x1, x2 + 1):
                grid[(x, coord.y1)] += 1

    result = len(list(filter(lambda n: n > 1, grid.values())))
    print('Day 5 part 1:', result)

    for coord in coordinates:
        if coord.x1 != coord.x2 and coord.y1 != coord.y2:
            dx = 1 if coord.x1 < coord.x2 else -1
            dy = 1 if coord.y1 < coord.y2 else -1
            steps = abs(coord.x1 - coord.x2)
            for i in range(steps + 1):
                grid[(coord.x1 + i * dx, coord.y1 + i * dy)] += 1

    result = len(list(filter(lambda n: n > 1, grid.values())))
    print('Day 5 part 2:', result)


if __name__ == '__main__':
    main(*sys.argv[1:])
