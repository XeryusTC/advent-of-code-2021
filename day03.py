from pathlib import Path
import sys
from typing import List

def to_base10(num: List[int]) -> int:
    result = 0
    for n, bit in enumerate(reversed(num)):
        result += bit * 2 ** n
    return result


def get_frequencies(reports: List[List[int]]) -> List[int]:
    frequencies = [0] * len(reports[0])
    for report in reports:
        for i, bit in enumerate(report):
            frequencies[i] += int(bit)
    return frequencies


def main(infile: str):
    input_ = Path(infile).read_text()
    reports = input_.splitlines()
    reports = [list(map(int, report)) for report in reports]

    frequencies = get_frequencies(reports)

    epsilon = []
    gamma = []
    for freq in frequencies:
        if freq > (len(reports) / 2):
            gamma.append(1)
            epsilon.append(0)
        else:
            gamma.append(0)
            epsilon.append(1)

    gamma_10 = to_base10(gamma)
    epsilon_10 = to_base10(epsilon)

    print('Day 3 part 1:', gamma_10 * epsilon_10)

    def filter_o2(ratings, n):
        # Base case
        if n > len(gamma) or len(ratings) == 1:
            return ratings[0]

        frequencies = get_frequencies(ratings)
        keep = 1 if frequencies[n] - len(ratings) / 2 >= 0 else 0

        filtered = filter(lambda r: int(r[n]) == keep, ratings)
        return filter_o2(list(filtered), n + 1)

    def filter_co2(ratings, n):
        # Base case
        if n > len(gamma) or len(ratings) == 1:
            return ratings[0]

        frequencies = get_frequencies(ratings)
        keep = 0 if frequencies[n] - len(ratings) / 2 >= 0 else 1

        filtered = filter(lambda r: int(r[n]) == keep, ratings)
        return filter_co2(list(filtered), n + 1)


    o2 = list(filter_o2(reports, 0))
    co2 = filter_co2(reports, 0)

    o2_10 = to_base10(o2)
    co2_10 = to_base10(co2)

    print('Day 3 part 2:', o2_10 * co2_10)


if __name__ == '__main__':
    main(*sys.argv[1:])
