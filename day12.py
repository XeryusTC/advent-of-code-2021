from collections import defaultdict, Counter
import copy
from pathlib import Path
import sys
from typing import Dict, List, Set, Tuple


def islower(string: str) -> bool:
    return all(c.islower() for c in string)


def search(
    graph: Dict[str, Set],
    closed: defaultdict[str, int],
    node: str,
    path: Tuple[str],
    max_visits=1,
) -> List[List[str]]:
    if node == 'end':
        return [path + (node,)]

    if islower(node):
        closed[node] += 1

    if max_visits > 1 and len([v for v in closed.values() if v >= max_visits]) > 1:
        return ()

    results = []
    for neighbour in graph[node]:
        if closed[neighbour] >= max_visits or neighbour == 'start':
            continue

        result = search(
            graph,
            copy.copy(closed),
            neighbour,
            path + (node,),
            max_visits
        )
        results += result
    return results


def main(infile: str):
    input = Path(infile).read_text().splitlines()
    graph = defaultdict(set)
    for line in input:
        v1, v2 = line.split('-', 1)
        graph[v1].add(v2)
        graph[v2].add(v1)
    result = search(graph, defaultdict(int), 'start', ())
    print('Day 12 part 1:', len(result))

    result = search(graph, defaultdict(int), 'start', (), max_visits=2)
    print('Day 12 part 2:', len(result))

if __name__ == '__main__':
    main(sys.argv[1])
