from pathlib import Path
import sys
from typing import List

def main(infile: str):
    input_: str = Path(infile).read_text()
    movements: List[str] = input_.splitlines()

    depth = 0
    position = 0
    for movement in movements:
        action, distance = movement.split(maxsplit=1)
        if action == 'forward':
            position += int(distance)
        elif action == 'up':
            depth -= int(distance)
        elif action == 'down':
            depth += int(distance)
        else:
            raise ValueError(movement)

    print(f'Day 2 part 1: {depth * position}')

    depth = 0
    position = 0
    aim = 0

    for movement in movements:
        action, distance = movement.split(maxsplit=1)
        distance = int(distance)
        if action == 'forward':
            position += distance
            depth += distance * aim
        elif action == 'up':
            aim -= distance
        elif action == 'down':
            aim += distance
        else:
            raise ValueError(movement)

    print(f'Day 2 part 2: {depth * position}')


if __name__ == '__main__':
    main(*sys.argv[1:])
