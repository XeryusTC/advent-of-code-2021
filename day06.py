from collections import Counter
import copy
from pathlib import Path
import sys
from typing import List


def simulate(days: int, fishes: List[int]) -> int:
    fishes = Counter(fishes)
    for _ in range(days):
        new_fishes = {}
        for age, count in fishes.items():
            if age == 0:
                try:
                    new_fishes[6] += count
                except KeyError:
                    new_fishes[6] = count
                new_fishes[8] = count
            else:
                try:
                    new_fishes[age - 1] += count
                except KeyError:
                    new_fishes[age - 1] = count
        fishes = new_fishes
    return sum(fishes.values())


def main(infile: str):
    input_ = list(map(int, Path(infile).read_text().split(',')))

    print('Day 6 part 1:', simulate(80, input_))
    print('Day 6 part 2:', simulate(256, input_))

if __name__ == '__main__':
    main(*sys.argv[1:])
