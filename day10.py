from pathlib import Path
import sys

PAIRS = {
    ')': '(',
    '}': '{',
    ']': '[',
    '>': '<',
}

SCORES = {
    '(': 3,
    ')': 3,
    '[': 57,
    ']': 57,
    '{': 1197,
    '}': 1197,
    '<': 25137,
    '>': 25137,
}

COMPLETE = {v: k for k, v in PAIRS.items()}
COMPLETION_SCORES = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
}


def main(infile:str):
    lines = Path(infile).read_text().splitlines()

    scores = []
    part1 = 0
    for line in lines:
        stack = []
        for c in line:
            # Part 1
            if c in '([{<':
                stack.append(c)
                continue

            o = stack.pop(-1)
            if o == PAIRS[c]:
                continue

            part1 += SCORES[c]
            break
        else:
            # part 2
            score = 0
            completion = reversed([COMPLETE[c] for c in stack])
            for c in completion:
                score = score * 5 + COMPLETION_SCORES[c]
            scores.append(score)

    print('Day 10 part 1:', part1)
    print('Day 10 part 2:', list(sorted(scores))[int(len(scores) / 2)])


if __name__ == '__main__':
    main(sys.argv[1])
