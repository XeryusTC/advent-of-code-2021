from functools import reduce
from pathlib import Path
import sys
from typing import List, Tuple


def dijkstra(map: List[List[int]], x: int, y: int) -> int:
    closed = set()
    open = set([(x, y)])

    while open:
        x, y = open.pop()
        closed.add((x, y))
        for new_x, new_y in neighbours(map, x, y):
            if map[new_y][new_x] == 9:
                continue
            if (new_x, new_y) not in closed:
                open.add((new_x, new_y))
    return len(closed)


def neighbours(map: List[List[int]], x: int, y: int) -> List[Tuple[int, int]]:
    ns = []
    if x > 0:
        ns.append((x - 1, y))
    if y > 0:
        ns.append((x, y - 1))
    if x < len(map[0]) - 1:
        ns.append((x + 1, y))
    if y < len(map) - 1:
        ns.append((x, y + 1))

    return ns


def main(infile: str):
    map = [[int(c) for c in line] for line in Path(infile).read_text().splitlines()]

    low_sum = 0
    basin_sizes = []
    for y in range(len(map)):
        for x in range(len(map[0])):
            if x > 0 and map[y][x] >= map[y][x - 1]:
                continue
            if x < len(map[0]) - 1 and map[y][x] >= map[y][x + 1]:
                continue
            if y > 0 and map[y][x] >= map[y - 1][x]:
                continue
            if y < len(map) - 1 and map[y][x] >= map[y + 1][x]:
                continue

            low_sum += map[y][x] + 1
            basin_sizes.append(dijkstra(map, x, y))

    print('Day 9 part 1:', low_sum)
    print('Day 9 part 2:', reduce(lambda x, y: x * y, sorted(basin_sizes)[-3:]))



if __name__ == '__main__':
    main(sys.argv[1])
