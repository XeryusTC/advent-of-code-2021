import itertools
from pathlib import Path
import sys

DIGITS = {
    0: 'abcefg',
    1: 'cf',
    2: 'acdeg',
    3: 'acdfg',
    4: 'bcdf',
    5: 'abdfg',
    6: 'abdefg',
    7: 'acf',
    8: 'abcdefg',
    9: 'abcdfg',
}


def main(infile: str):
    input_ = Path(infile).read_text()
    inputs = []
    outputs = []
    for line in input_.splitlines():
        in_, out = line.split('|', 1)
        inputs.append([i.strip() for i in in_.split()])
        outputs.append([o.strip() for o in out.split()])

    combined_outputs = itertools.chain(*outputs)
    filtered_outputs = filter(lambda o: len(o) in (2, 4, 3, 7), combined_outputs)
    print('Day 8 part 1:', len(list(filtered_outputs)))

    result = 0
    for in_, out in zip(inputs, outputs):
        numbers = {}
        for number in in_:
            if len(number) == 2:
                numbers[1] = number
            elif len(number) == 4:
                numbers[4] = number
            elif len(number) == 3:
                numbers[7] = number
            elif len(number) == 7:
                numbers[8] = number

        for number in in_:
            if len(number) == 6: # 0, 6 or 9
                if all(d in number for d in numbers[4]):
                    numbers[9] = number
                elif all(d in number for d in numbers[1]):
                    numbers[0] = number
                else:
                    numbers[6] = number
        for number in in_:
            if len(number) == 5: # 2, 3, 5
                if all(d in number for d in numbers[7]):
                    numbers[3] = number
                elif all(d in numbers[6] for d in number):
                    numbers[5] = number
                else:
                    numbers[2] = number

        numbers = {''.join(sorted(v)): k for k, v in numbers.items()}

        output = 0
        for o in out:
            output = output * 10 + numbers[''.join(sorted(o))]
        result += output

    print('Day 8 part 2:', result)

if __name__ == '__main__':
    main(sys.argv[1])
