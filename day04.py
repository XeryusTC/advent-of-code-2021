from copy import deepcopy
from pathlib import Path
import sys

WIDTH = 5
HEIGHT = 5


class Board:
    def __init__(self, board):
        self.board = board
        self.checked = [
            [False, False, False, False, False],
            [False, False, False, False, False],
            [False, False, False, False, False],
            [False, False, False, False, False],
            [False, False, False, False, False],
        ]

    def print(self):
        for row in self.board:
            for number in row:
                print(f'{number:>2}', end=' ')
            print()

        for row in self.checked:
            for check in row:
                print(check, end=' ')
            print()

    def mark(self, number):
        for y, row in enumerate(self.board):
            for x, n in enumerate(row):
                if n == number:
                    self.checked[y][x] = True

    def done(self) -> bool:
        for row in self.checked:
            if all(row):
                return True
        for x in range(WIDTH):
            all_checked = True
            for y in range(HEIGHT):
                if not self.checked[y][x]:
                    all_checked = False
            if all_checked:
                return True

        return False

    def unmarked_sum(self) -> int:
        sum = 0
        for y in range(HEIGHT):
            for x in range(WIDTH):
                if not self.checked[y][x]:
                    sum += self.board[y][x]
        return sum


def part1(numbers, boards) -> int:
    for number in numbers:
        for board in boards:
            board.mark(number)
            if board.done():
                return number * board.unmarked_sum()


def part2(numbers, boards) -> int:
    for number in numbers:
        for board in boards:
            board.mark(number)
        left = len(list(filter(lambda b: not b.done(), boards)))
        if left == 1:
            break

    for board in boards:
        if not board.done():
            break

    for number in numbers:
        board.mark(number)
        if board.done():
            return number * board.unmarked_sum()

    return 0


def main(infile: str):
    file = Path(infile)
    with file.open() as f:
        input_ = list(map(lambda l: l.strip(), f.readlines()))

        numbers = list(map(int, input_[0].split(',')))

        board = []
        boards = []
        for line in input_[2:]:
            if not line:
                boards.append(Board(board))
                board = []
                continue
            board.append(list(map(int, filter(lambda l: l != '', line.split(' ')))))


        result = part1(numbers, deepcopy(boards))
        print('Day 4 part 1:', result)
        result = part2(numbers, deepcopy(boards))
        print('Day 4 part 2:', result)


if __name__ == '__main__':
    main(*sys.argv[1:])
